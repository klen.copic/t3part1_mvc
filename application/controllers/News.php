<?php
//https://www.studenti.famnit.upr.si/~klen/CI_121021/index.php/News/view/test

class News extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('news_model');
                $this->load->helper('url_helper');
                $this->load->helper('form');
                       
        }

        public function index()
        {
                $data['news'] = $this->news_model->get_news();
                $data["title"] = "News archive";

                $this->load->view("templates/header", $data);

                $this->load->view("news", $data);

                $this->load->view("templates/footer");

        }

        public function view($slug = NULL)
        {
                $data['news_item'] = $this->news_model->get_news($slug);
                $data["title"] = "News Title";
                //var_dump($data['news_item']);
                $this->load->view("templates/header", $data);
                $this->load->view("news/view", $data);
                $this->load->view("templates/footer");
        }

        public function create()
        {

                $this->load->library("form_validation");

                $this->form_validation->set_rules('title','Title', 'required');
                $this->form_validation->set_rules('text','Text', 'required');

                if($this->form_validation->run() === FALSE){
                        $data["title"] = "Create a news item";
                        $this->load->view("templates/header", $data);
                        $this->load->view("news/create");
                        $this->load->view("templates/footer");       
                }else{
                        //echo "data posted!";
                        $data["title"] = "News added succesfully ";
                        $this->news_model->save_news();
                        $this->load->view("templates/header", $data);
                        $this->load->view("templates/footer");       
                }
                
        }
}